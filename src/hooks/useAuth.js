export default function () 
{
  const [state, dispatch] = React.useReducer((state,action)=>{

    switch(action.type){
  
      case 'SET_USER':
      return {

        ...state,
        user:{...action.payload},
  
      };
  
      default:
        return state;
  
    }
  
  },

  {

    user:undefined,
  
},
);
  
  

const auth = React.useMemo(  
  ()=>({
  login: async (email,password)=>{

    //await sleep(2000);
    //console.log('login',email,password);

    const {data} = await Axios.post('https://syncshopper.syncsound.pk/api/login', {  
    moboremail: email,
      password: password,
    });
// console.log(data.token);
    const user = {
      email: data.data.user.email,
      token: data.data.token,
    };
    dispatch(createAction('SET_USER',user));

  // console.log(user);
  },

  logout: ()=>{

    console.log("logout");
  },

}),
[],
);

// console.log(state.user);
}