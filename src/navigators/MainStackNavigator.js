/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import {Home} from '../screens/Home';
import {NewFIR} from '../screens/NewFIR'
// import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Icon } from 'react-native-vector-icons/Ionicons';




// const MainStack = createStackNavigator();
const Drawer = createDrawerNavigator();


export function MainStackNavigator() 
{
  return(

      <Drawer.Navigator
      
      >
      <Drawer.Screen name="Home" component={Home}
      />
      <Drawer.Screen name="NewFIR" component={NewFIR}/>
      </Drawer.Navigator>

  )
}
