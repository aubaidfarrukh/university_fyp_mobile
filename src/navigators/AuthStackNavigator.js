/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import {LoginScreen} from '../screens/LoginScreen';
import { createStackNavigator } from '@react-navigation/stack';



const AuthStack = createStackNavigator();


export function AuthStackNavigator() 
{
  return(

      <AuthStack.Navigator screenOptions={{
        headerShown:false
      }}>
      <AuthStack.Screen name={'LoginScreen'} component={LoginScreen}/>
      </AuthStack.Navigator>

  )
}
