import React from 'react';
import { View, Text, Button, TextInput, StyleSheet } from 'react-native';
import { color } from 'react-native-reanimated';

export function Heading({children,style, ...props})
{

return <Text{...props} style={[styles.text,style]}> {children} </Text>
} 

const styles= StyleSheet.create({
    text:{
        fontSize:32,
        color:'black'

    }
});