import React from 'react';
import { View, Text, Button, TextInput, StyleSheet } from 'react-native';
import { color } from 'react-native-reanimated';

export function Input({style, ...props})
{

return <TextInput{...props} style={[styles.input,style]}/>
} 

const styles= StyleSheet.create({
    input:{

        backgroundColor:'#e8e8e8',
        width:'100%',
        padding: 20,
        borderRadius:8,

    }
});