import React from 'react';
import { View, Text, Button, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import { color } from 'react-native-reanimated';

export function FilledButton({title,style,onPress})
{

return(

    <TouchableOpacity style={[styles.container,style]} onPress={onPress}>
        <Text style={[styles.text]}>{title.toUpperCase()}</Text>
    </TouchableOpacity>

)
} 


const styles= StyleSheet.create({

    container:{
        backgroundColor: '#10222e',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        borderRadius:8
    },

    text:{
        color:'white',
        fontWeight:'500',
        fontSize: 18
    }
})