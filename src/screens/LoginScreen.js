import 'react-native-gesture-handler';
import React, { Component, useState } from 'react';
import { View, Text, Button, TextInput, StyleSheet } from 'react-native';
import {Heading} from '../components/heading';
import {Input } from '../components/input';
import { Home } from '../screens/Home';
import { FilledButton } from '../components/FilledButton';
import { AuthContext } from '../context/AuthContext';
import { Loading } from '../components/Loading';


export function LoginScreen({navigation})
{

  const { auth: {login} } = React.useContext(AuthContext);
  const [email,setemail] = React.useState('');
  const [password,setpassword] = React.useState('');
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState('');


  return (
    <View style={Styles.container}>
    <Heading style={Styles.title}>Criminal record system</Heading>
    {/* <Error error={error} /> */}
    <Input style={Styles.input} placeholder={'Email'} keyboardType={'email-address'} value={email} onChangeText={setemail}/>
    <Input style={Styles.inputpassword} placeholder={'Password'} secureTextEntry value={password} onChangeText={setpassword}/>
    <FilledButton 
    title={'Login'} 
    style={Styles.loginButton} 
    onPress={ async ()=>{

//      alert("hello");
      try{
        //console.log(loading);
        setLoading(true);
        await login(email,password);
      }
      catch(e)
      {
        setError(e.message);
        setLoading(false);

      }
    }}/>

 <Loading loading={loading} />



    </View>
 
  );
}

const Styles = StyleSheet.create({

  container:{
    flex: 1,
    paddingTop:120,
    padding: 20,
    alignItems:'center'
  },

  title:{
    marginBottom:15,
  },
  
  inputpassword:{
    marginTop:-9
  },

  input:{
    marginVertical: 20
  }
})