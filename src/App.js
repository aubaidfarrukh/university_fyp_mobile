/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React, { Component } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {AuthStackNavigator} from './navigators/AuthStackNavigator';
import {MainStackNavigator} from './navigators/MainStackNavigator';
// import { createDrawerNavigator } from '@react-navigation/drawer';
import { AuthContext } from './context/AuthContext';
import { Input } from './components/input';
import { sleep } from './utils/sleep';
import Axios from 'axios';
import { createAction } from './config/createAction';


const RootStack = createStackNavigator();
// const drawerStack = createDrawerNavigator();

export default function () 
{

  const [state, dispatch] = React.useReducer((state,action)=>{

    switch(action.type){
  
      case 'SET_USER':
      return {

        ...state,
        user:{...action.payload},
  
      };
  
      default:
        return state;
  
    }
  
  },

  {

    user:undefined,
  
},
);
  
  

const auth = React.useMemo(  
  ()=>({
  login: async (email,password)=>{

    //await sleep(2000);
    //console.log('login',email,password);

    const {data} = await Axios.post('https://syncshopper.syncsound.pk/api/login', {  
    moboremail: email,
      password: password,
    });
// console.log(data.token);
    const user = {
      user: data.data.user.name,
      email: data.data.user.email,
      token: data.data.token,
    };
    dispatch(createAction('SET_USER',user));

  // console.log(user);
  },

  logout: ()=>{

    console.log("logout");
  },

}),
[],
);

console.log(state.user);

return(

    <AuthContext.Provider value={{auth,user:state.user}}>
    <NavigationContainer>
      <RootStack.Navigator screenOptions={{
        headerShown:false
      }}>
        {
    state.user ?(<RootStack.Screen name={'MainStrack'} component={MainStackNavigator}/>)
     : (<RootStack.Screen name={'RootStrack'} component={AuthStackNavigator}/>)
      
        }
      </RootStack.Navigator>
    </NavigationContainer>
    </AuthContext.Provider>
  );
}
