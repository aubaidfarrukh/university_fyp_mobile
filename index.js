/**
 * @format
 */

import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
// import login from './src/screens/Login';
// import home from './src/screens/Home';


AppRegistry.registerComponent(appName, () => App);
